using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Feedbacks;

public class GameEnd : MonoBehaviour
{
    [SerializeField]
    private MMFeedbacks _sceneTransition;

    private BoxCollider _col;

    private void Start()
    {
        _col = GetComponent<BoxCollider>();

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Player>())
        {
            StartCoroutine(GameOver());
            _col.enabled = false;
        }
    }

    IEnumerator GameOver()
    {
        yield return new WaitForSeconds(5f);
        _sceneTransition.PlayFeedbacks();
    }

}
