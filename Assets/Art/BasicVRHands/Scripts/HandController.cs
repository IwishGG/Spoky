using UnityEngine;
using System.Collections;
using HTC.UnityPlugin.Vive;

public partial class HandController : MonoBehaviour
{
    [SerializeField]
    private HandType _handtype;

    private Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (_handtype == HandType.Left)
        {
            animator.SetBool("isGrabbing", ViveInput.GetPressEx(HandRole.LeftHand, ControllerButton.Trigger));
        }
        if (_handtype == HandType.Right)
        {
            animator.SetBool("isGrabbing", ViveInput.GetPressEx(HandRole.RightHand, ControllerButton.Trigger));
        }

    }
}
