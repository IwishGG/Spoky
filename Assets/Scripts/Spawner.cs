using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    [SerializeField]
    private GameObject _cube;
    [SerializeField]
    private GameObject _obstacle;
    [SerializeField]
    private Transform[] _spawnPoints;
    [SerializeField]
    private float[] _spawnTimer;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnRandomCube());
        StartCoroutine(SpawnRandomObstacle());
    }

    IEnumerator SpawnRandomCube()
    {
        yield return new WaitForSeconds(_spawnTimer[Random.Range(0, _spawnTimer.Length)]);
        SpawnCube(_cube, Random.Range(0, _spawnPoints.Length), Random.Range(0, 4));
    }

    private void SpawnCube(GameObject cubeObj, int positionNum, int rotationNum)
    {

        var cube = Instantiate(cubeObj, _spawnPoints[positionNum].position, _spawnPoints[positionNum].rotation, transform);

        if (positionNum == 1)
        {
            if (rotationNum == 1)
            {
                rotationNum = 2;
            }
            if (rotationNum == 3)
            {
                rotationNum = 4;
            }
        }
        cube.transform.Rotate(-transform.forward, 90 * rotationNum);

        StartCoroutine(SpawnRandomCube());
    }

    private void SpawnObstacle(GameObject obstacleObj, int positionNum)
    {
        var obstacle = Instantiate(obstacleObj, _spawnPoints[positionNum].position, _spawnPoints[positionNum].rotation, transform);
        StartCoroutine(SpawnRandomObstacle());
    }


    IEnumerator SpawnRandomObstacle()
    {
        yield return new WaitForSeconds(20);
        SpawnObstacle(_obstacle, Random.Range(0, _spawnPoints.Length));
    }

    // Update is called once per frame
    void Update()
    {

    }
}
