using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{
    [SerializeField]
    private float _cubeSpeed;
    public bool IsScliced = false;

    void Update()
    {
        transform.position += Time.deltaTime * transform.forward * _cubeSpeed;
    }
   


}
