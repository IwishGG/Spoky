using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    [SerializeField]
    private float _obstaleSpeed;

    void Update()
    {
        transform.position += Time.deltaTime * transform.forward * _obstaleSpeed;
    }

}
