using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static int Score;
    public static int Combo;
    public static bool IsGameOver;
    public static float PlayerHealth;

    [SerializeField]
    private TextMeshProUGUI _score;
    [SerializeField]
    private TextMeshProUGUI _combo;
    [SerializeField]
    private Slider _healthbar;
    [SerializeField]
    private AudioSource _bgm;
    [SerializeField]
    private float _playerHealth = 500;
    [SerializeField]
    private GameObject _endUI;
    [SerializeField]
    private GameObject _pointer;


    // Start is called before the first frame update
    void Start()
    {
        IsGameOver = false;
        PlayerHealth = _playerHealth;
        Time.timeScale = 1f;
        _endUI.gameObject.SetActive(false);
        _pointer.gameObject.SetActive(false);
        Score = 0;
    }

    // Update is called once per frame
    void Update()
    {
        _score.text = $"{Score}";
        _combo.text = $"{Combo}";
        _healthbar.value = PlayerHealth / _playerHealth;

        if (_bgm.isPlaying == false && IsGameOver == false)
        {
            GameOver();
        }
        if (PlayerHealth <= 0 && IsGameOver == false)
        {
            GameOver();
        }

        PlayerHealth = Mathf.Clamp(PlayerHealth, 0, _playerHealth);
        Score = Mathf.Clamp(Score, 0, 999999);
    }

    private void GameOver()
    {
        Time.timeScale = 0f;
        IsGameOver = true;
        _endUI.gameObject.SetActive(true);
        _pointer.gameObject.SetActive(true);
    }
    public void PlayAgain()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }
    public void Song2()
    {
        SceneManager.LoadScene("Song2");
    }
    public void Song1()
    {
        SceneManager.LoadScene("Song1");

    }
    public void BTMainMenu()
    {

        SceneManager.LoadScene("MainMenu");
    }
}
