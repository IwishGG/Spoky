using HTC.UnityPlugin.Vive;
using UnityEngine;
using HTC.UnityPlugin.Utility;


public class FloatDash : MonoBehaviour
{
    [SerializeField]
    private Transform _camera;
    [SerializeField]
    private float _jumpSpeed;
    [SerializeField]
    private float _dashSpeed;
    void Update()
    {
        RigidPose pose1 = VivePose.GetPoseEx(HandRole.RightHand);
        RigidPose pose2 = VivePose.GetPoseEx(TrackerRole.Tracker1);
        if (ViveInput.GetPressEx(HandRole.RightHand, ControllerButton.Trigger))
        {
            transform.localPosition += transform.up * _jumpSpeed * Time.deltaTime;
        }
        if (ViveInput.GetPressUpEx(HandRole.RightHand, ControllerButton.Trigger))
        {
            transform.localPosition += _camera.forward * _dashSpeed;
        }
    }
}
