using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HTC.UnityPlugin.Vive;

public class MoveWithLocoMotion : MonoBehaviour
{
    [SerializeField]
    private Transform _rig;
    [SerializeField]
    private Transform _camera;
    [SerializeField]
    private float _speed = 1.5f;
    [SerializeField]
    private Vector2 _input;

    private Rigidbody _rb;
    private Vector3 _direction;

    private void Start()
    {
        _rb = _rig.GetComponent<Rigidbody>();
    }

    private void Update()
    {

        var forward = _camera.forward;
        forward.y = 0;
        var right = _camera.right;
        right.y = 0;
        _input = ViveInput.GetPadAxis(HandRole.RightHand);
        _direction = forward * _input.y + right * _input.x;
        _direction = _direction.normalized;
        _rig.position += new Vector3(_direction.x, 0, _direction.z * 0.5f) * _speed * Time.deltaTime;

    }


}
