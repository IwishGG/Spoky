using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadBody : MonoBehaviour
{
    [SerializeField]
    private GameObject _fx;
    [SerializeField]
    private AudioSource _audio;
    [SerializeField]
    private GameObject _blood;
    [SerializeField]
    private Animator _animator;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Brain>())
        {
            _fx.SetActive(false);
            _audio.Stop();
            _blood.SetActive(false);
            Destroy(other.gameObject, 0.1f);
            _animator.SetTrigger("Idel");
        }
    }
}
