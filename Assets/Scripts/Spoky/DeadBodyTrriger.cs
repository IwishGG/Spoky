using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class DeadBodyTrriger : MonoBehaviour
{
    [SerializeField]
    private GameObject _fx;
    [SerializeField]
    private AudioSource _audio;
    [SerializeField]
    private GameObject _blood;
    [SerializeField]
    private Animator _animator;
    [SerializeField]
    private GameObject _analytics;
    private BoxCollider _col;

    // Start is called before the first frame update
    void Start()
    {
        _fx.SetActive(false);
        _blood.SetActive(false);
        _col = GetComponent<BoxCollider>();
        _col.enabled = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Player>())
        {
            _fx.SetActive(true);
            _audio.PlayOneShot(_audio.clip);
            _blood.SetActive(true);
            _col.enabled = false;
            _animator.SetTrigger("Shake");
            _analytics.SetActive(true);
        }
    }
}
