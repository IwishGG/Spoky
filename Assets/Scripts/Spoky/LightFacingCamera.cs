using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFacingCamera : MonoBehaviour
{
    private Camera _cam;
    private void Start()
    {
        _cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(_cam.transform);
    }
}
