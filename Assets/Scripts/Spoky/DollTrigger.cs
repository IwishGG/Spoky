using UnityEngine;
using UnityEngine.Analytics;

public class DollTrigger : MonoBehaviour
{

    [SerializeField]
    private Doll[] _dolls;
    [SerializeField]
    private BoxCollider _col;
     [SerializeField]
    private GameObject _analytics;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Player>())
        {
            foreach (var item in _dolls)
            {
                if (item.IsActive == false)
                {
                   _analytics.SetActive(true);
                    item._anim.SetTrigger("WakeUp");
                }
            }
            _col.enabled = false;
        }
    }
}
