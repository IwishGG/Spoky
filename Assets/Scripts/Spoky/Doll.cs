using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Doll : MonoBehaviour
{
    public bool IsActive;
    private Player _player;
    public Animator _anim;
    public NavMeshAgent NavAgent;
    private Camera _mainCamera;
    [SerializeField]
    private AudioSource _audio;
    // Start is called before the first frame update
    void Start()
    {
        _player = LevelManager.instance;
        _mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (IsActive)
        {
            transform.LookAt(_player.transform.position, Vector3.up);
            NavAgent.SetDestination(_player.transform.position);
        }
        else
        {
            return;
        }
    }
    public void SetActive()
    {
        IsActive = true;
    }

    public void PlayFX()
    {
        _audio.PlayOneShot(_audio.clip);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.GetComponent<Brain>())
        {
            IsActive = false;
            _audio.Stop();
            _anim.SetTrigger("Idel");
            Destroy(other.gameObject, 0.1f);
        }
    }

}
