using HTC.UnityPlugin.ColliderEvent;
using HTC.UnityPlugin.Utility;
using System.Collections.Generic;
using UnityEngine;

public class Lighter : MonoBehaviour,
    IColliderEventPressEnterHandler
{

    [SerializeField]
    private GameObject _light;
    [SerializeField]
    private bool _isHold;
    [SerializeField]
    private ColliderButtonEventData.InputButton m_activeButton = ColliderButtonEventData.InputButton.Trigger;

    private HashSet<ColliderButtonEventData> pressingEvents = new HashSet<ColliderButtonEventData>();
    public ColliderButtonEventData.InputButton activeButton { get { return m_activeButton; } set { m_activeButton = value; } }
    // Start is called before the first frame update
    void Start()
    {

    }

    public void OnColliderEventPressEnter(ColliderButtonEventData eventData)
    {
        if (eventData.button == m_activeButton && pressingEvents.Add(eventData) && pressingEvents.Count == 1)
        {
            // _isHold = !_isHold;
            // SetLight();
        }
    }

    public void OpenLight()
    {
        _light.SetActive(true);
    }
    public void CloseLight()
    {
        _light.SetActive(false);
    }

}
