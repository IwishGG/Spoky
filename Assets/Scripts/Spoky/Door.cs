using HTC.UnityPlugin.ColliderEvent;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class Door : MonoBehaviour, IColliderEventPressEnterHandler
{

    [SerializeField]
    private ColliderButtonEventData.InputButton m_activeButton = ColliderButtonEventData.InputButton.Trigger;
    [SerializeField]
    private Animator _anim;
    [SerializeField]
    private float _closeTimer = 10f;
    [SerializeField]
    private GameObject _analytics;

    private HashSet<ColliderButtonEventData> pressingEvents = new HashSet<ColliderButtonEventData>();
    public ColliderButtonEventData.InputButton activeButton { get { return m_activeButton; } set { m_activeButton = value; } }

    public void OnColliderEventPressEnter(ColliderButtonEventData eventData)
    {
        if (eventData.button == m_activeButton && pressingEvents.Add(eventData) && pressingEvents.Count == 1)
        {
            _anim.SetTrigger("OpenDoor");
            _analytics.SetActive(true);
            StartCoroutine(CloseDoor());
        }
    }

    IEnumerator CloseDoor()
    {
        yield return new WaitForSeconds(_closeTimer);
        _anim.SetTrigger("CloseDoor");
    }

}
