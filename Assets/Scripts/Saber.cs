using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Saber : MonoBehaviour
{
    [SerializeField]
    private float _cutAngle = 90;
    [SerializeField]
    private AudioSource _audioGood;
    [SerializeField]
    private AudioSource _audioBad;
    private int _comboMult = 1;

    public enum SaberType
    {
        right,
        left
    }

    public SaberType SaberHand = SaberType.right;
    private Vector3 _previousPos;



    // Update is called once per frame
    void Update()
    {
        SaberRay();
        _previousPos = transform.position;
    }

    private void SaberRay()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.up, out hit, 1.7f))
        {
            if (CutOnAngle(hit, this.transform.position, _previousPos))
            {
                var cube = hit.collider.GetComponent<Cube>();
                if (SaberHand == SaberType.right && hit.collider.GetComponent<Cube>())
                {
                    if (cube.IsScliced == false)
                    {
                        if (hit.collider.gameObject.CompareTag("Blue"))
                        {
                            GoodSlice();
                        }
                        else if (hit.collider.gameObject.CompareTag("Red"))
                        {
                            BadSlice();
                        }
                        cube.IsScliced = true;
                    }
                }
                else if (SaberHand == SaberType.left && hit.collider.GetComponent<Cube>())
                {
                    if (cube.IsScliced == false)
                    {
                        if (hit.collider.gameObject.CompareTag("Red"))
                        {
                            GoodSlice();
                        }
                        else if (hit.collider.gameObject.CompareTag("Blue"))
                        {
                            BadSlice();
                        }
                        cube.IsScliced = true;
                    }
                }
            }
            else
            {
                if (hit.collider.GetComponent<Cube>())
                {
                    var cube = hit.collider.GetComponent<Cube>();
                    if (hit.collider.gameObject.CompareTag("Blue") || hit.collider.gameObject.CompareTag("Red"))
                    {
                        if (cube.IsScliced == false)
                        {
                            BadSlice();
                            cube.IsScliced = true;
                        }
                    }

                }
            }
            if (hit.collider.gameObject.CompareTag("Obstacle"))
            {
                BadSlice();
                Destroy(hit.collider.gameObject);
            }
        }
    }

    private bool CutOnAngle(RaycastHit hit, Vector3 currentPos, Vector3 previousPos)
    {
        if (Vector3.Angle(currentPos - previousPos, hit.transform.up) > _cutAngle)
        {
            Debug.Log(Vector3.Angle(currentPos - previousPos, hit.transform.up));
            return true;
        }
        else

            return false;
    }

    private void GoodSlice()
    {
        _audioGood.PlayOneShot(_audioGood.clip);
        ComboExtraScore();
        GameManager.Score += 100 * _comboMult;
        GameManager.Combo++;
        GameManager.PlayerHealth += 20;

    }
    private void BadSlice()
    {

        _audioBad.PlayOneShot(_audioBad.clip);
        GameManager.Score -= 100;
        GameManager.Combo = 0;
        GameManager.PlayerHealth -= 100;
    }
    private void ComboExtraScore()
    {
        if (GameManager.Combo >= 10 && GameManager.Combo < 20)
        {
            _comboMult = 2;
        }
        else if (GameManager.Combo >= 20 && GameManager.Combo < 30)
        {
            _comboMult = 3;
        }
        else if (GameManager.Combo >= 30)
        {
            _comboMult = 4;
        }
        else
        {
            _comboMult = 1;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawRay(transform.position, transform.up * 1.7f);
    }
}
