using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [SerializeField]
    private GameObject _controls;

    private void Start()
    {
        _controls.SetActive(false);
    }
    public void LoadGame()
    {
        SceneManager.LoadScene("Spoky");
    }
    public void Controls()
    {
        _controls.SetActive(true);
    }
    public void Back()
    {
        _controls.SetActive(false);
    }
    public void Quit()
    {
        Application.Quit();
    }
}
