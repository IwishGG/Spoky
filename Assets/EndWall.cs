using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndWall : MonoBehaviour
{
    [SerializeField]
    private AudioSource _audio;
    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.GetComponent<Cube>())
        {
            _audio.PlayOneShot(_audio.clip);
            GameManager.Score -= 100;
            GameManager.Combo = 0;
            GameManager.PlayerHealth -= 100;
            Destroy(other.gameObject, 2f);
        }
        if (other.gameObject.CompareTag("Obstacle"))
        {
            Destroy(other.gameObject);
        }
    }
}
